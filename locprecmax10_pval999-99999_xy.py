# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 15:31:03 2022

@author: Alistair Curd, University of Leeds
"""

import os
import numpy as np
import pandas as pd
from tkinter import Tk
from tkinter.filedialog import askdirectory

Tk().withdraw()
# Choose input folder
inputfolder = askdirectory(title='Please select input folder')
# Choose output folder
outputfolder = askdirectory(title='Please select output folder')

# Filter data for localisation precision and ONI NimOS p-value
# WARNING!! P-VALUE IS INVERTED IN NIMOS COMPARED WITH CODI!!
for file in os.listdir(inputfolder):
    if file.endswith('.csv'):
        # Load
        print(file)
        data = pd.read_csv(os.path.join(inputfolder, file))
        print(repr(len(data)) + ' locs')
        # Filter for precision
        data = data[data['locprecnm_max'] < 10]
        # Filter for p-value
        data = data[np.logical_and(
            data['p-value'] > 0.999, data['p-value'] < 0.99999
            )]
        print('-> ' +repr(len(data))+ 'locs.')
        # Select channels
        data = data[['xnm', 'ynm']]
        # Save
        data.to_csv(os.path.join(outputfolder,
                file[0:-4]+'_pr10_pv999-99999_xy.csv'
                )
            , index=False
            )
