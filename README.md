# switch-processing-formats #

For converting between data formats for data processing, especially single molecule localisation data.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

### How do I get set up? ###

Needs pandas.

Run add_locprecnm.py to add a single localisation precision (nm) column to all ONI data files in a directory. SMAP will then be able to read this.
Command line: ```python add_locprecnm.py```

With jupyter notebook, you can use *switching-format-options.ipynb* to save .csv files containing localisation data with new or selected columns.

### Who do I talk to? ###

Alistair Curd, University of Leeds
