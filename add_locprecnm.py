# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 15:31:03 2022

@author: Alistair Curd, University of Leeds
"""

import os
import pandas as pd
from tkinter import Tk
from tkinter.filedialog import askdirectory

Tk().withdraw()
# Choose input folder
inputfolder = askdirectory(title='Please select input folder')
# Choose output folder
outputfolder = askdirectory(title='Please select output folder')

# Add locprecnm column to all data and save
# Locprecnm is a combination of X precision and Y precision in the ONI files
# Include max of X and Y, e.g. for filtering; mean of x and y e.g. for estimation
for file in os.listdir(inputfolder):
    if file.endswith('.csv'):
        print(file)
        data = pd.read_csv(os.path.join(inputfolder, file))
        data['locprecnm_mean'] = (data[['X precision (nm)', 'Y precision (nm)']].mean(axis=1))
        data['locprecnm_max'] = (data[['X precision (nm)', 'Y precision (nm)']].max(axis=1))
        data = data[['Channel', 'Frame', 'X (nm)', 'Y (nm)', 'Photons', 'Background', 'p-value', 'locprecnm_mean', 'locprecnm_max']]
        data.columns = ['channel', 'frame', 'xnm', 'ynm', 'phot', 'bg', 'p-value', 'locprecnm_mean', 'locprecnm_max']
        data.to_csv(os.path.join(outputfolder,
                                 file[0:-4]+'_locprecnm.csv'
                                 )
                    , index=False
                    )
